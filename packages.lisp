(in-package :cl-user)

(defpackage :lotabout.euler
  (:use :cl)
  (:export :num->digits
	   :digits->num
	   :sieve-odds))

(load "lotabout.euler.lisp")
