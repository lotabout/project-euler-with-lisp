;; Problem 16:

;; 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
;; 
;; What is the sum of the digits of the number 2^1000?


(defun add-digits (n)
  (multiple-value-bind (f r) (floor n 10)
    (if (zerop f)
	r
	(+ r (add-digits f)))))

(defun pro16 ()
  (add-digits (expt 2 1000)))
