;;; Problem 31:


;; In England the currency is made up of pound, £, and pence, p, and
;; there are eight coins in general circulation:
;; 
;;     1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
;; 
;; It is possible to make £2 in the following way:
;; 
;;     1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
;; 
;; How many different ways can £2 be made using any number of coins?


;; IDEA:
; equivalent problem is shown below:
; a*x_1 + b*x_2 + ... + n*x_n = t
; so iterate all possible and then all possible b and so on.

(defun find-ways (num possbl-lst)
  (let ((possbl-lst-sort (sort possbl-lst #'>)))
    (labels ((rec (num lst)
	       (let ((cur-coin (first lst))
		     (rest-lst (rest lst)))
		 (cond
		   ((= num 0) 1)
		   ((or (< num 0) (not cur-coin)) 0)
		   (t
		    (loop for i from 0 upto (floor num cur-coin)
		       sum (rec (- num (* i cur-coin)) rest-lst)))))))
      (rec num possbl-lst-sort))))

(defun pro31 ()
  (find-ways 200 '(1 2 5 10 20 50 100 200)))
