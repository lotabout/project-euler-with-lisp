;;; problem 30:


;; Surprisingly there are only three numbers that can be written as
;; the sum of fourth powers of their digits:
;; 
;;     1634 = 1^4 + 6^4 + 3^4 + 4^4
;;     8208 = 8^4 + 2^4 + 0^4 + 8^4
;;     9474 = 9^4 + 4^4 + 7^4 + 4^4
;; 
;; As 1 = 14 is not a sum it is not included.
;; 
;; The sum of these numbers is 1634 + 8208 + 9474 = 19316.
;; 
;; Find the sum of all the numbers that can be written as the sum of
;; fifth powers of their digits.

; IDEA(in chinese)
; 当一个数字出现的位数高于一定程度时，它的5次方和会比它小，例如：
; 200>32=2^5,此例中意味着只出现2时，大于等于200的数肯定不符合条件。
; 而每个小于某位的数可以为后面的数提供补偿，如20中的2可以提供32-20=12的
; 补偿。最后可算得，数字9能提供最大的补偿，为4*9^5-9999=226197,得到补偿
; 后，一些数字可以越位，如2得到168的补偿后就可以处于第3位(即200)而不违
; 反规则，可得数字1吸收的补偿最少，故符合条件的最大数应在239999左右（不
; 超过）。

(defun count-fifth-power (num)
  (labels ((rec (num sum)
	     (multiple-value-bind (quotient remainer) (floor num 10)
	       (if (and (= quotient 0) (= remainer 0))
		   sum
		   (rec quotient (+ sum (expt remainer 5)))))))
    (rec num 0)))

; note that all numbers that obey this rule will not exceed 239999
(defun pro30 ()
  (loop for i from 2 to 239999
     when (= i (count-fifth-power i))
       sum i))
