;;; Problem 35:

;; The number, 197, is called a circular prime because all rotations
;; of the digits: 197, 971, and 719, are themselves prime.
;; 
;; There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17,
;; 31, 37, 71, 73, 79, and 97.
;; 
;; How many circular primes are there below one million?

(defun sieve-of-eratosthenes (maximum)
  (let ((sieve (make-array (1+ maximum) :element-type 'bit
                                          :initial-element 0))
	(sieve-hash (make-hash-table)))
    (loop for candidate from 2 to maximum
       when (zerop (bit sieve candidate))
       do (setf (gethash candidate sieve-hash) 't)
       and do (loop for composite from (expt candidate 2) 
		 to maximum by candidate
		 do (setf (bit sieve composite) 1)))
    sieve-hash))

(defun num->digits (num)
  (labels ((rec (num lst)
	     (if (= num 0)
		 lst
		 (multiple-value-bind (quotient remainer) (floor num 10)
		   (rec quotient (cons remainer lst))))))
    (rec num '())))

(defun digits->num (lst)
  (labels ((rec (lst num)
	     (if (null lst)
		 num
		 (rec (cdr lst) (+ (* 10 num) (car lst))))))
    (rec lst 0)))

(defun rotate (list count)
  (if (minusp count)
      (rotate list (+ (length list) count))
      (nconc (subseq list count) (subseq list 0 count))))

(defun get-rotate-nums (num)
  (let ((digits (get-digits num)))
    (loop for i from 0 upto (1- (length digits))
       collect (digits->num (rotate digits i)))))

(defun all (lst)
  (cond
    ((null lst) t)
    ((car lst) (all (cdr lst)))
    (t nil)))

(defun pro35 (&optional (max 1000000))
  (let ((sieve (sieve-of-eratosthenes max)))
    (loop for k being the hash-key in sieve using (hash-value v)
       when (all (mapcar #'(lambda (x) (gethash x sieve)) (get-rotate-nums k)))
	 collect k)))
