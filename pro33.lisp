;;; Problem 33:


;; The fraction 49/98 is a curious fraction, as an inexperienced
;; mathematician in attempting to simplify it may incorrectly believe
;; that 49/98 = 4/8, which is correct, is obtained by cancelling the
;; 9s.
;; 
;; We shall consider fractions like, 30/50 = 3/5, to be trivial examples.
;; 
;; There are exactly four non-trivial examples of this type of
;; fraction, less than one in value, and containing two digits in the
;; numerator and denominator.
;; 
;; If the product of these four fractions is given in its lowest
;; common terms, find the value of the denominator.

(defun get-digits (num)
  (labels ((rec (num lst)
	     (multiple-value-bind (quotient remainer) (floor num 10)
	       (if (= quotient 0)
		   (cons remainer lst)
		   (rec quotient (cons remainer lst))))))
    (rec num '())))

(defun get-simplify (a b)
  (labels ((rec (lst-a lst-b res-a res-b)
	     (let ((head-a (car lst-a))
		   (head-b (car lst-b)))
	       (cond
		 ((not head-a) (values res-a (append (reverse res-b) lst-b)))
		 ((not head-b) (values (append (reverse res-a) lst-a) res-b))
		 ((= head-a head-b) (rec (cdr lst-a) (cdr lst-b) res-a res-b))
		 ((< head-a head-b) (rec (cdr lst-a) lst-b (cons head-a res-a) res-b))
		 (t (rec lst-a (cdr lst-b) res-a (cons head-b res-b)))))))
    (rec (sort a #'<) (sort b #'<) '() '())))

(defun find-numbers ()
  (loop for i from 11 upto 99
     when (not (= (mod i 10) 0))
     append (loop for j from (+ i 1) upto 99
	       when (not (= (mod j 10) 0))
	       append
		 (let ((digits-i (get-digits i))
		       (digits-j (get-digits j)))
		   (unless (null (set-difference digits-i digits-j))
		     (multiple-value-bind (a b)
			   (get-simplify (get-digits i) (get-digits j))
			 (when (and (= (length a) 1) (= (length b) 1)
				    (= (/ (first a) (first b)) (/ i j)))
;			   (list `(,i ,j))))))))) ; return the rational pair
			   (list (/ i j)))))))))

(defun pro33 ()
  (denominator (apply #'* (find-numbers))))
