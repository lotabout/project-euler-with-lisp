;;; common.lisp
;;; commonly used functions
(in-package :lotabout.euler)

(defun num->digits (num &optional (base 10))
  "explode a number into digits, using a specified base.
1234 => (1 2 3 4)[base 10]"
  (labels ((rec (num lst)
	     (if (= num 0)
		 lst
		 (multiple-value-bind (quotient remainer) (floor num base)
		   (rec quotient (cons remainer lst))))))
    (if (= num 0)
	(list 0)
	(rec num '()))))

(defun digits->num (lst &optional (base 10))
  "splice a list of digits into a number, using a specified base.
   (1 2 3 4) => 1234[base 10]"
  (labels ((rec (lst num)
	     (if (null lst)
		 num
		 (rec (cdr lst) (+ (* base num) (car lst))))))
    (rec lst 0)))

(defun sieve-of-eratosthenes (maximum)
  "using sieve of eratosthenes to generate primes below 'maximum'
=> (2 3 5 7 ...)"
  (let ((sieve (make-array (1+ maximum) :element-type 'bit
                                          :initial-element 0)))
    (loop for candidate from 2 to maximum
          when (zerop (bit sieve candidate))
            collect candidate
            and do (loop for composite from (expt candidate 2) 
                                         to maximum by candidate
                          do (setf (bit sieve composite) 1)))))

(defun sieve-odds (maximum) "sieve for odd numbers"
  (cons 2 
        (let ((maxi (ash (1- maximum) -1)) (stop (ash (isqrt maximum) -1)))
          (let ((sieve (make-array (1+ maxi) :element-type 'bit :initial-element 0)))
            (loop for i from 1 to maxi
              when (zerop (sbit sieve i))
              collect (1+ (ash i 1))
              and when (<= i stop) do
                (loop for j from (ash (* i (1+ i)) 1) to maxi by (1+ (ash i 1))
                   do (setf (sbit sieve j) 1)))))))
