;;; Problem 17:

;; If the numbers 1 to 5 are written out in words: one, two, three,
;; four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in
;; total.
;; 
;; If all the numbers from 1 to 1000 (one thousand) inclusive were
;; written out in words, how many letters would be used?
;; 
;; NOTE: Do not count spaces or hyphens. For example, 342 (three
;; hundred and forty-two) contains 23 letters and 115 (one hundred and
;; fifteen) contains 20 letters. The use of "and" when writing out
;; numbers is in compliance with British usage.

;; using common lisp `format' with "~r"

(defun count-str (str &optional (ignore '(#\space #\-)))
  (length (remove-if #'(lambda (x) (member x ignore))
		     str)))

(defun count-num (num)
  (let ((str (format nil "~r" num)))
    (+ (count-str str)
					; add 3 for "and" 
       ; note that 100 and 1000 do not need for that 
       (if (< num 100) 0
	   (if (= (mod num 100) 0) 0 3))))) 

(defun pro17 (&optional (max 1000))
  (loop for i from 1 upto max
     sum (count-num i)))
  
