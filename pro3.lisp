;; Proble 3:
;; The prime factors of 13195 are 5, 7, 13 and 29.
;; What is the largest prime factor of the number 600851475143 ?

(defun list-primes (&optional (sieve 1000000))
  (let ((numbers (make-array sieve :initial-element t))
	(upper-bound (isqrt (1- sieve)))
	(lst '()))
    (do ((i 2 (1+ i)))
	((> i upper-bound))
      (when (svref numbers i)
	(push i lst)
	(do ((j (* i i) (+ j i)))
	    ((> j (1- sieve)))
	  (setf (svref numbers j) nil))))
    (do ((i (1+ upper-bound) (1+ i)))
	((= i sieve) lst)
      (when (svref numbers i)
	(push i lst)))))

(defun pro3 (&optional (n 600851475143))
  (let ((primes (list-primes (isqrt n))))
    (dolist (i primes)
      (when (zerop (mod n i))
	(return-from pro3 i)))))

; another implementation of Eratosthenes sieve
; from wikipedia
(defun sieve-of-eratosthenes (maximum)
  (let ((sieve (make-array (1+ maximum) :element-type 'bit
			   :initial-element 0)))
    (loop for candidate from 2 to maximum
	 when (zerop (bit sieve candidate))
	 collect candidate
	 and do (loop for composite from (expt candidate 2)
		     to maximum by candidate
		     do (setf (bit sieve composite) 1)))))
