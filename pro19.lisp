;;; Problem: 19

;; You are given the following information, but you may prefer to do some research for yourself.
;; 
;;     1 Jan 1900 was a Monday.
;;     Thirty days has September,
;;     April, June and November.
;;     All the rest have thirty-one,
;;     Saving February alone,
;;     Which has twenty-eight, rain or shine.
;;     And on leap years, twenty-nine.
;;     A leap year occurs on any year evenly divisible by 4, but not on a
;;     century unless it is divisible by 400.
;; 
;; How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

(defun leap? (year)
  (if (= (mod year 100) 0)
    (= (mod year 400) 0)
    (= (mod year 4) 0)))

;; count how many days from 1900/1/1
;; year must be >= 1901
(defun days-year (year)
  (if (> year 1900)
    (+ (* (- year 1900) 365) (floor (- year 1901) 4))
    0))

(defun days-month (year month)
  ; initialize
  (let ((shift `(1 ,(- (if (leap? year) 1 0) 2) 1 0 1 0 1 1 0 1 0 1 )))
    (+ (* (1- month) 30)
       (apply #'+ (subseq shift 0 (1- month))))))

(defun days (year month day)
  (+ (days-year year) (days-month year month) (1- day)))

(defun weekday (year month day)
  (mod (days year month day) 7))

(defun pro19 ()
  (loop for year from 1901 upto 2000
        sum (loop for month from 1 upto 12
                  when (= (weekday year month 1) 6)
                  sum 1)))
