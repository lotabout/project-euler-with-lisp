;;; Problem 27:

;; n^2 + an + b
;; Find the product of the coefficients, a and b, for the quadratic
;; expression that produces the maximum number of primes for
;; consecutive values of n, starting with n = 0.

(defun is-prime (num)
  (when (> num 1)
    (let ((max-bound (isqrt num)))
      (do ((i 2 (1+ i)))
	  ((> i max-bound) t)
	(if (= (mod num i) 0)
	    (return nil))))))

(defun how-many-primes (a b)
  (do ((n 0 (1+ n))
       (num 0 (1+ num)))
      ((not (is-prime (+ (* n n) (* a n) b)))
       num)))

(defun get-max-idx (a-bound b-bound)
  (let ((max 0)
	(max-idx-a)
	(max-idx-b))
    (do ((b 2 (1+ b)))
	((> b b-bound) (values `(,max-idx-a ,max-idx-b) max))
      (do ((a (- 0 a-bound) (1+ a)))
	  ((> a a-bound))
	(let ((num-of-primes (how-many-primes a b)))
	  (if (> num-of-primes max)
	      (setf max num-of-primes
		    max-idx-a a
		    max-idx-b b)))))))

(defun pro27 (&optional (a-bound 1000) (b-bound 1000))
  (apply #'* (get-max-idx a-bound b-bound)))
