;; Problem 23:

;; A perfect number is a number for which the sum of its proper
;; divisors is exactly equal to the number. For example, the sum of
;; the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which
;; means that 28 is a perfect number.
;; 
;; A number n is called deficient if the sum of its proper divisors is
;; less than n and it is called abundant if this sum exceeds n.
;; 
;; As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the
;; smallest number that can be written as the sum of two abundant
;; numbers is 24. By mathematical analysis, it can be shown that all
;; integers greater than 28123 can be written as the sum of two
;; abundant numbers. However, this upper limit cannot be reduced any
;; further by analysis even though it is known that the greatest
;; number that cannot be expressed as the sum of two abundant numbers
;; is less than this limit.
;; 
;; Find the sum of all the positive integers which cannot be written
;; as the sum of two abundant numbers.

;; find divisors of a number
(defun divisors (n &aux (lows '()) (highs '()))
  (do ((divisor 1 (1+ divisor)))
      ((>= (* divisor divisor) n)
       (when (= (* divisor divisor) n)
	 (push divisor highs))
       (nreconc lows highs))
    (multiple-value-bind (quotient remainder) (floor n divisor)
      (when (zerop remainder)
	(push divisor lows)
	(push quotient highs)))))

(defun get-abundant-list (&optional (limit 28123))
  (labels ((abundantp (n)
	     (if (> (apply #'+ (divisors n)) (* 2 n)) t nil)))
    (loop for i from 1 upto limit when (abundantp i) collect i)))

(defun abundant-sum (abundant-lst &optional (limit most-positive-fixnum))
  (let ((sum-table (make-hash-table)))
    (loop for i in abundant-lst
     do (loop for j in abundant-lst
	   do (when (<= (+ i j) limit)
		(setf (gethash (+ i j) sum-table) t))))
    (loop for k being the hash-key in sum-table sum k)))

(defun pro23 (&optional (limit 28123))
  (- (floor (* limit (+ 1 limit)) 2)
     (abundant-sum (get-abundant-list limit) limit)))


