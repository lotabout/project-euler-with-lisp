;;; Problem:


;; The decimal number, 585 = 10010010012 (binary), is palindromic in
;; both bases.
;; 
;; Find the sum of all numbers, less than one million, which are
;; palindromic in base 10 and base 2.
;; 
;; (Please note that the palindromic number, in either base, may not
;; include leading zeros.)

(defun num->digits (num &optional (base 10))
  (labels ((rec (num lst)
	     (if (= num 0)
		 lst
		 (multiple-value-bind (quotient remainer) (floor num base)
		   (rec quotient (cons remainer lst))))))
    (if (= num 0)
	(list 0)
	(rec num '()))))

(defun digits->num (lst &optional (base 10))
  (labels ((rec (lst num)
	     (if (null lst)
		 num
		 (rec (cdr lst) (+ (* base num) (car lst))))))
    (rec lst 0)))


(defun gen-pal (num &optional (even t))
  "even:t => 132 -> 132231
event:nil => 132 -> 13231"
  (let ((digits (num->digits num)))
    (if even
	(digits->num (nconc digits (reverse digits)))
	(digits->num (nconc digits (cdr (reverse digits)))))))


(defun is-palindromic (lst)
  (equal lst (reverse lst)))

; quick version (1000 times faster)
; iterate from 1 to 1000 for each number, generate its corresponding
; palindromic number: 123 -> 12321 && 123321
(defun pro36 (&optional (max 1000000))
  (let* ((num-of-digits (floor (log max 10)))
	 (max-num (expt 10 (ceiling num-of-digits 2)))
	 (res '()))
    (do ((i 1 (1+ i)))
	((>= i max-num) res)
      (let ((even-pal (gen-pal i t))
	    (odd-pal (gen-pal i nil)))
	(progn
	  (if (and (> max even-pal)
		   (is-palindromic (num->digits even-pal 2)))
	      (push even-pal res))
	  (if (and (> max odd-pal)
		   (is-palindromic (num->digits odd-pal 2)))
	      (push odd-pal res)))))
    (apply #'+ res)))

; slow version
(defun pro36-test (&optional (max 1000000))
  (apply #'+
	 (loop for i from 1 below max
	    when (and (is-palindromic (num->digits i 10))
		      (is-palindromic (num->digits i 2)))
	    collect i)))
