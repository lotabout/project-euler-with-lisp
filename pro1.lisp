; using iteration
(defun sum-of-3-and-5 (limit)
  (loop for i from 1 to limit
     if (or (= 0 (mod i 3))
	    (= 0 (mod i 5)))
     sum i))

(defun pro1 ()
  (sum-of-3-and-5 999))
