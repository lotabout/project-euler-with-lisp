;; Problem 10:

;; The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

;; Find the sum of all the primes below two million.

; from wikipedia
(defun sieve-of-eratosthenes (maximum)
  (let ((sieve (make-array (1+ maximum) :element-type 'bit
			   :initial-element 0)))
    (loop for candidate from 2 to maximum
	 when (zerop (bit sieve candidate))
	 collect candidate
	 and do (loop for composite from (expt candidate 2)
		     to maximum by candidate
		     do (setf (bit sieve composite) 1)))))

(defun pro10 (&optional (max 2000000))
  (loop for i in (sieve-of-eratosthenes max)
     sum i))
