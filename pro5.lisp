;; Problem 5:
;; 2520 is the smallest number that can be divided by each of the numbers
;; from 1 to 10 without any remainder.  What is the smallest positive
;; number that is evenly divisible by all of the numbers from 1 to 20?

;; Use three different methods.


;; First: get all prime factors of all numbers and multiply them
;; 8 12 => ((2 2 2) (2 2 3)) => ((2 . 3) (3 . 1)) => 2^3 * 3^1

; generate primes using sieve
; from wikipedia
(defun sieve-of-eratosthenes (maximum)
  (let ((sieve (make-array (1+ maximum) :element-type 'bit
			   :initial-element 0)))
    (loop for candidate from 2 to maximum
	 when (zerop (bit sieve candidate))
	 collect candidate
	 and do (loop for composite from (expt candidate 2)
		     to maximum by candidate
		     do (setf (bit sieve composite) 1)))))

(defun prime-factors (n)
  "find prime factors of a number.
10 -> (2 5)
99 -> (3 3 11)"
  (let ((primes (sieve-of-eratosthenes n)))
    (labels ((factors (num)
	       (if (> num 1)
		   (do ((i 1 (1+ i)))
		       ('nil)
		     (let ((prime (elt primes (1- i))))
		       (when (zerop (mod num prime))
			 (return (cons prime (factors (floor num prime))))))))))
      (factors n))))

(defun group-factors (xs)
  "(2 2 2 3 5 5) => ((2 . 3) (3 . 1) (5 . 2))"
  (let ((lst '())) ; result list
    (dolist (x xs)
      (if (assoc x lst)
	  (incf (cdr (assoc x lst)))
	  (push `(,x . 1) lst)))
    lst))

(defun common-factors (num-lst)
  "(8 12) -> ((2 . 3) (3 . 1))"
  (let ((all-factors '()))
    (dolist (x num-lst)
      (let ((factors-of-x (group-factors (prime-factors x))))
	(dolist (factor factors-of-x)
	  (let ((factor-in-alls (assoc (car factor) all-factors)))
	    (cond ((not factor-in-alls)
		   (push factor all-factors))
		  ((> (cdr factor) (cdr factor-in-alls))
		   (setf (cdr factor-in-alls) (cdr factor))))))))
    all-factors))

(defun my-lcm (num-lst)
  "leaset common multiple:
8 12 => 24"
  (let ((res 1))
    (dolist (factor (common-factors num-lst))
      (setf res (* res (expt (car factor) (cdr factor)))))
    res))

(defun pro5 (&optional (n 20))
  (my-lcm (loop for i from 1 to n collect i)))

;; Second: using built in lcm
(defun pro5-2 (&optional (n 20))
  (apply #'lcm (loop for i from 1 to n collect i)))

;; Third: (lcm (lcm first second) third)
(defun lcm-two (a b)
  "lcm (a b) = a*b / gcd(a b)"
  (floor (* a b) (gcd a b)))

(defun pro5-3 (n)
  (let ((res 1))
    (loop for i from 1 to n
       do (setf res (lcm-two res i)))
    res))
