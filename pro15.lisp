;; Problem 15:

;; see http://projecteuler.net/problem=15

;; Note that for mxn grid, the number of all possible routes is
;; C_(m+n)^(m)

(defun pro15 (&optional (a 20) (b a))
  (let ((m (+ a b))
	(n a))
    (labels ((factorial (n)
	       (if (<= n 1)
		   1
		   (* n (factorial (1- n))))))
      (/ (factorial m) (factorial n) (factorial b)))))
