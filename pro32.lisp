;;; Problem 32:

;; We shall say that an n-digit number is pandigital if it makes use of all the
;; digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1
;; through 5 pandigital.
;; 
;; The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing
;; multiplicand, multiplier, and product is 1 through 9 pandigital.
;; 
;; Find the sum of all products whose multiplicand/multiplier/product identity
;; can be written as a 1 through 9 pandigital.
;; HINT: Some products can be obtained in more than one way so be sure to only
;; include it once in your sum.

;; IDEA:
;; Note that A*B=C, and (A U B U C) = {1 2 3 4 5 6 7 8 9} contains only 9 digits.
;; so the length must be: 1|4=4 or 2|3=4, otherwise it will not fit.
;; I've generated all permutations of [1-9] and check whether a permutation 
;; with a model 1|4=4 or 2|3=4 fit the properties required.
;;
;; Example: permutaion: (3 9 1 8 6 7 2 5 4) (given by the problem)
;; (3 | 9 1 8 6 | 7 2 5 4) => 3*9186 != 7254 => not fit
;; (3 9 | 1 8 6 | 7 2 5 4) => 39*186 == 7254 => fit

; permutation
(defun perm (lst)
  "(1 2 3) -> ((1 2 3) (1 3 2) (2 1 3) (2 3 1) (3 1 2) (3 2 1))"
  (flet ((con-lst (elmnt perm-lst)
           (mapcar #'(lambda (l) (cons elmnt l)) perm-lst)))
    (if (null lst)
      '(())
      (loop for e in lst
        append (con-lst e (perm (remove e lst)))))))

(defun digits->num (lst)
  "(1 2 3 4) -> 1234"
  (labels ((rec (l acc)
             (if (null l)
               acc
               (rec (cdr l) (+ (* acc 10) (car l))))))
    (rec lst 0)))

(defun pandigit? (lst)
  (if (or (= (* (digits->num (subseq lst 0 1)) (digits->num (subseq lst 1 5)))
             (digits->num (subseq lst 5 9)))
          (= (* (digits->num (subseq lst 0 2)) (digits->num (subseq lst 2 5)))
             (digits->num (subseq lst 5 9))))
    (digits->num (subseq lst 5 9)) 
    0))

(defun pro32 ()
  (apply #'+ (remove-duplicates (loop for x in (perm '(1 2 3 4 5 6 7 8 9))
        collect (pandigit? x)))))
