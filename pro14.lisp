;; Problem 14:

;; The following iterative sequence is defined for the set of positive integers:
;; 
;; n → n/2 (n is even)
;; n → 3n + 1 (n is odd)
;; 
;; Using the rule above and starting with 13, we generate the following sequence:
;; 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
;; 
;; It can be seen that this sequence (starting at 13 and finishing at
;; 1) contains 10 terms. Although it has not been proved yet (Collatz
;; Problem), it is thought that all starting numbers finish at 1.
;; Which starting number, under one million, produces the longest chain?
;; 
;; NOTE: Once the chain starts the terms are allowed to go above one million.


;; Use hash table to speed up.

(defparameter *all-chain-num* (make-hash-table))

(defun len-of-chain (n)
  (if (gethash n *all-chain-num*)
      (gethash n *all-chain-num*)
      (if (eql n 1)
	  (progn
	    (setf (gethash 1 *all-chain-num*) 1)
	    1)
	  (let* ((next (if (evenp n) (/ n 2) (1+ (* 3 n))))
		 (len (len-of-chain next)))
	    (setf (gethash n *all-chain-num*) (1+ len))
	    (1+ len))))) 

(defun pro14 (&optional (times 1000000))
  (let ((max 1)
	(max-item 1))
    (dotimes (x (1- times))
      (let ((len (len-of-chain (1+ x))))
	(if (<= max len)
	    (progn
	      (setf max len)
	      (setf max-item x)))))
    (values max-item max)))
