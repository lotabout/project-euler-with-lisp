;;; Problem 24:


;; A permutation is an ordered arrangement of objects. For example,
;; 3124 is one possible permutation of the digits 1, 2, 3 and 4. If
;; all of the permutations are listed numerically or alphabetically,
;; we call it lexicographic order. The lexicographic permutations of
;; 0, 1 and 2 are:
;; 
;; 012   021   102   120   201   210
;; 
;; What is the millionth lexicographic permutation of the digits 0, 1,
;; 2, 3, 4, 5, 6, 7, 8 and 9?

; Idea:
; n numbers will yeild n! permutations. thus, for [0..9]'s
; lexicographic permutations, if the first element is 0, then all the
; rest will have 9! possibilities.
; [0] [XXXXXXXXX] cover the 1th to the 9!th permutations.
; [1] [XXXXXXXXX] cover the (9!+1)th to the (2*9!)th permutaions.
; ...
; so the millionth permutation would have '2' as its first element.
; => 725760 = (2*9!) < 1000,000 < (3*9!+1) = 1088640
; and so on ...

(defun nth-permutation (index elements)
  (labels ((fact (n)
	     (apply #'* (loop for i from 1 upto n collect i)))
	   (rec (idx elmnts prefix)
	     (if (null elmnts)
		 prefix
		 (let* ((num-of-permutation (fact (1- (length elmnts))))
			(idx-of-elmnts (floor (1- idx) num-of-permutation))
			(e (nth idx-of-elmnts elmnts)))
		   (rec (- idx (* idx-of-elmnts num-of-permutation))
			(remove e elmnts)
			(append prefix (list e)))))))
    (rec index elements '())))

(defun pro24 ()
  (nth-permutation 1000000
		   (loop for i from 0 upto 9 collect i)))
