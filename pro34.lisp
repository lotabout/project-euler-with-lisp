;;; Problem 34:


;; 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.
;; 
;; Find the sum of all numbers which are equal to the sum of the
;; factorial of their digits.
;; 
;; Note: as 1! = 1 and 2! = 2 are not sums they are not included.

; pre-calculate factorials will save a lot of time.

(defun fact (num)
  (apply #'* (loop for i from 1 upto num collect i)))

(defun digit-fact (n)
  (aref #(1 1 2 6 24 120 720 5040 40320 362880 3628800) n))

(defun get-digits (num)
  (labels ((rec (num lst)
	     (multiple-value-bind (quotient remainer) (floor num 10)
	       (if (= quotient 0)
		   (cons remainer lst)
		   (rec quotient (cons remainer lst))))))
    (rec num '())))

(defun is-curious (num)
;  (= num (apply #'+ (mapcar #'fact (get-digits num)))))
  (= num (apply #'+ (mapcar #'digit-fact (get-digits num)))))

(defun pro34 ()
  (apply #'+
	 (loop for i from 3 upto 9999999
	    when (is-curious i)
	    collect i)))
