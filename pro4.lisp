;; Problem 4
;; A palindromic number reads the same both ways. The largest palindrome
;; made from the product of two 2-digit numbers is 9009 = 91 × 99.
;; Find the largest palindrome made from the product of two 3-digit
;; numbers.

(defun palindromic-p (num)
  (let ((str (format nil "~A" num)))
    (string= str (reverse str))))

(defun list-palindromics (&optional (limit 99))
  "find all palindromic numbers less than limit*limit including numbers with a single digit"
  (let ((palindromics '()))
    (do ((i 1 (1+ i)))
	((> i limit) palindromics)
      (do ((j i (1+ j)))
	  ((> j limit))
	(let ((mul (* i j)))
	  (when (palindromic-p mul)
	    (push mul palindromics)))))))

(defun pro4 (digits)
  (let ((limits (1- (expt 10 digits))))
    (apply #'max (list-palindromics limits))))
