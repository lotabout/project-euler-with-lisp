;;; Problem 26:



;; A unit fraction contains 1 in the numerator. The decimal
;; representation of the unit fractions with denominators 2 to 10 are
;; given:
;; 
;;     1/2	= 	0.5
;;     1/3	= 	0.(3)
;;     1/4	= 	0.25
;;     1/5	= 	0.2
;;     1/6	= 	0.1(6)
;;     1/7	= 	0.(142857)
;;     1/8	= 	0.125
;;     1/9	= 	0.(1)
;;     1/10	= 	0.1
;; 
;; Where 0.1(6) means 0.166666..., and has a 1-digit recurring
;; cycle. It can be seen that 1/7 has a 6-digit recurring cycle.
;; 
;; Find the value of d < 1000 for which 1/d contains the longest
;; recurring cycle in its decimal fraction part.

;; Below are thought about this problem (in chinese):
; 手工找到一个数的倒数的小数循环位数，如7
; 括号[]内为 [余数,出现顺序]
; 1/7 => 0.0[1,1] => 0.1[3,2] => 0.14[2,3] => 0.142[6,4] =>
; 0.1428[4,5] => 0.14285[5,6] => 0.142857[1,7]
; 因为余数1已经出现过，所以长度为(7-1=6)

(defun get-remain (a b)
  "auto padding zero when divide
11 / 23 => 110 / 23 => (4 18)"
  (do* ((i a (* i 10))
	(quotient (floor i b) (floor i b)))
       ((> quotient 0) (floor i b))))

(defun get-recycle (num)
  (let ((recycle-list '((1 1))))
    (do ((cur-remain (caar recycle-list) (caar recycle-list))
	 (cur-idx 1 (1+ cur-idx)))
	(nil)
      (multiple-value-bind (quotient remain) (get-remain cur-remain num)
	(if (= remain 0)
	    (return 0)
	    (let ((found (assoc remain recycle-list)))
	      (if found
		  (return (1+ (- cur-idx (cadr found))))
		  (push (list remain cur-idx) recycle-list))))))))

(defun pro26 (&optional (limit 1000))
  (let ((max 1)
	(max-num 1))
    (do ((i 1 (1+ i)))
	((>= i limit) max-num)
      (let ((recycle-num (get-recycle i)))
	(if (> recycle-num max)
	    (setq max recycle-num max-num i))))))
