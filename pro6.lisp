;; Problem 6:

;; The sum of the squares of the first ten natural numbers is,
;; 1^2 + 2^2 + ... + 10^2 = 385
;; 
;; The square of the sum of the first ten natural numbers is,
;; (1 + 2 + ... + 10)^2 = 552 = 3025
;; 
;; Hence the difference between the sum of the squares of the first ten
;; natural numbers and the square of the sum is 3025 − 385 = 2640.
;; 
;; Find the difference between the sum of the squares of the first one
;; hundred natural numbers and the square of the sum.

;; First -> direct answer

(defun sum-of-squres (n)
  "1^2 + 2^2 + ... + n^2"
  (loop for i from 1 to n sum (* i i)))

(defun squre-of-sum (n)
  "(1 + 2+ ... + n)^2"
  (let ((tmp (loop for i from 1 to n sum i)))
    (* tmp tmp)))

(defun pro6 (&optional (n 100))
  (- (squre-of-sum n) (sum-of-squres n)))

;; 2. using formular
;; (1 + 2 + ... + n)^2 = (n*(n+1)/2)^2
;; 1^2 + 2^2 + ... + n^2 = n*(n+1)*(2n+1)/6
;; => n*(n+1)*(3n^2 - n - 2)/12
(defun pro6-2 (&optional (n 100))
  (floor (* n (1+ n) (- (- (* 3 n n) n) 2)) 12))
