;;; Problem 37:


;; The number 3797 has an interesting property. Being prime itself, it
;; is possible to continuously remove digits from left to right, and
;; remain prime at each stage: 3797, 797, 97, and 7. Similarly we can
;; work from right to left: 3797, 379, 37, and 3.
;; 
;; Find the sum of the only eleven primes that are both truncatable
;; from left to right and right to left.
;; 
;; NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.

(defun num->digits (num &optional (base 10))
  "explode a number into digits, using a specified base.
1234 => (1 2 3 4)[base 10]"
  (labels ((rec (num lst)
	     (if (= num 0)
		 lst
		 (multiple-value-bind (quotient remainer) (floor num base)
		   (rec quotient (cons remainer lst))))))
    (if (= num 0)
	(list 0)
	(rec num '()))))

(defun digits->num (lst &optional (base 10))
  "splice a list of digits into a number, using a specified base.
(1 2 3 4) => 1234[base 10]"
  (labels ((rec (lst num)
	     (if (null lst)
		 num
		 (rec (cdr lst) (+ (* base num) (car lst))))))
    (rec lst 0)))

(defun sieve-odds (maximum) "sieve for odd numbers"
  (cons 2 
        (let ((maxi (ash (1- maximum) -1)) (stop (ash (isqrt maximum) -1)))
          (let ((sieve (make-array (1+ maxi) :element-type 'bit :initial-element 0)))
            (loop for i from 1 to maxi
              when (zerop (sbit sieve i))
              collect (1+ (ash i 1))
              and when (<= i stop) do
                (loop for j from (ash (* i (1+ i)) 1) to maxi by (1+ (ash i 1))
                   do (setf (sbit sieve j) 1)))))))

(defun prime? (num)
  (if (< num 2) nil
      (let ((limit (isqrt num)))
	(do ((i 2 (1+ i)))
	    ((> i limit) t)
	  (if (= (mod num i) 0)
	      (return nil))))))



(defun scan (lst &optional (right nil))
  "right=nil => (1 2 3) => ((1 2 3) (2 3) (3))
   right=t   => (1 2 3) => ((1 2 3) (1 2) (1))"
  (labels ((rec (lst ret)
	     (if (null lst)
		 (nreverse ret)
		 (if right
		     (rec (butlast lst) (cons lst ret))
		     (rec (cdr lst) (cons lst ret))))))
    (rec lst '())))

(defun ltrunc-prime? (num)
  (every #'prime? (mapcar #'digits->num (cdr (SCAN (num->digits num))))))

(defun rtrunc-prime? (num)
  (every #'prime? (mapcar #'digits->num (cdr (SCAN (num->digits num) t)))))

(defun pro37 (&optional (max 1000000))
  (apply #'+
	 (loop for i in (sieve-odds max)
	      when (and (> i 10) (rtrunc-prime? i) (ltrunc-prime? i))
	      collect i)))
