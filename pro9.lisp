;; Problem 9:

;; A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
;; a^2 + b^2 = c^2
;; 
;; For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
;; 
;; There exists exactly one Pythagorean triplet for which a + b + c = 1000.
;; Find the product abc.

(defun search-for-tripple (n)
  (loop for a from 1 to (floor n 3)
     do (loop for c from (floor n 3) to n
	   do (let ((b (- n a c)))
		(if (= (* c c)
		       (+ (* a a) (* b b)))
		    (return-from search-for-tripple `(,a ,b ,c))))))))))))

(defun pro9 (&optional (n 1000))
  (apply #'* (search-for-tripple n)))
