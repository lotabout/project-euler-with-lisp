; using recursion
(defun add-even-fibonacci (limit)
  (labels ((next-recursion (cur-fibo next-fibo sum limit)
	     (cond ((>= cur-fibo limit)
		    sum)
		   ((evenp cur-fibo)
		    (next-recursion next-fibo (+ cur-fibo next-fibo) (+ sum cur-fibo) limit))
		   (t
		    (next-recursion next-fibo (+ cur-fibo next-fibo) sum limit)))))
    (next-recursion 1 2 0 limit)))

(defun pro2 ()
  (add-even-fibonacci 3999999))
