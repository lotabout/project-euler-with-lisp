;; Problem 20:


;; n! means n × (n − 1) × ... × 3 × 2 × 1
;; 
;; For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
;; and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
;; 
;; Find the sum of the digits in the number 100!

(defun add-digits (n)
  (multiple-value-bind (f r) (floor n 10)
    (if (zerop f)
	r
	(+ r (add-digits f)))))

(defun factorial (n)
  (if (<= n 1)
      1
      (* n (factorial (1- n)))))

(defun pro20 (&optional (n 100))
  (add-digits (factorial n)))
