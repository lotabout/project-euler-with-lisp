;; Problem 7:

;; By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we
;; can see that the 6th prime is 13.  What is the 10 001st prime
;; number?

; from wikipedia
(defun sieve-of-eratosthenes (maximum)
  (let ((sieve (make-array (1+ maximum) :element-type 'bit
			   :initial-element 0)))
    (loop for candidate from 2 to maximum
	 when (zerop (bit sieve candidate))
	 collect candidate
	 and do (loop for composite from (expt candidate 2)
		     to maximum by candidate
		     do (setf (bit sieve composite) 1)))))


(defun pro7 (&optional (n 10001))
  (do ((max 2048 (* 2 max)))
      (nil)
    (let ((primes (sieve-of-eratosthenes max)))
      (if (> (length primes) n)
	  (return (elt primes (1- n)))
	  nil))))


; with predict
(defun pro7-2 (&optional (n 10001))
  (let ((primes (sieve-of-eratosthenes 200000)))
    (elt primes (1- n))))
