;; Problem 22

;; Using names.txt (right click and 'Save Link/Target As...'), a 46K
;; text file containing over five-thousand first names, begin by
;; sorting it into alphabetical order. Then working out the
;; alphabetical value for each name, multiply this value by its
;; alphabetical position in the list to obtain a name score.

;; For example, when the list is sorted into alphabetical order,
;; COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name
;; in the list. So, COLIN would obtain a score of 938 × 53 = 49714.
;; 
;; What is the total of all the name scores in the file?

(defun str-to-list (str &optional (sep '(#\space #\" #\,)))
  "(\"abc|||de|fg\" => (\"abc\" \"de\" \"fg\")"
  (let ((cur-word '())
	(res '()))
    (loop for i across (reverse str)
       do (if (member i sep)
	      (if cur-word
		  (progn
		    (push (coerce cur-word 'string) res)
		    (setf cur-word '())))
	      (push i cur-word)))
    (if cur-word (push (coerce cur-word 'string) res))
    res))

(defun count-score (str index)
  (labels ((char-index (c)
	     (1+ (- (char-int c) (char-int #\A)))))
    (* index (loop for c across str sum (char-index c)))))

(defun count-list (str)
  (loop for i in str
     for j from 1
     sum (count-score i j)))

(defun pro22 ()
  (with-open-file (fp "pro22.txt" :direction :input)
    (loop for line = (read-line fp nil nil)
       while line sum (count-list (sort (str-to-list line) #'string< )))))
